<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@m!n</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body>

    <form action="" method="POST" enctype="multipart/form-data">
        <input type="file" name="file" />
        <input type="submit"/>
    </form>

    <?php

        include './vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';
        $db = new SQLite3('fars-examination-db.db');
        $result = $db->query('DELETE FROM user');

        if(isset($_FILES['file'])){
            $file_name = $_FILES['file']['name'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_info = new SplFileInfo($file_name);
            
            $uploadPath = getcwd() . "/uploads/" . basename($file_name);
            $extensions= array("xls","xlsb","xlsm", "xlsx");
            
            if(in_array($file_info->getExtension(), $extensions) === false){
                echo "please choose an excel file.";
            } else {
                move_uploaded_file($file_tmp, $uploadPath);
                $rows;
                if ( $xlsx = SimpleXLSX::parse($uploadPath) ) {
                    $rows = $xlsx->rows();
                    
                    for($x = 0; $x < sizeof($rows); $x++){
                        $imagePath = 'images/image' . $x . '.jpg';
                        $img = imagecreate(400, 50);
            
                        $textbgcolor = imagecolorallocate($img, 173, 230, 181);
                        $textcolor = imagecolorallocate($img, 0, 192, 255);

                        $txt = 'index: ' . ($x + 1) . ' name: ' . $rows[$x][0];
                        $phone = $rows[$x][1];
                        imagettftext ($img , 22 , 0 , 10 , 35 , $textcolor , "./IRANSans.ttf" , $txt );

                        ob_start();
                        imagejpeg($img);

                        $query = "INSERT INTO user(phone, pic) VALUES ('$phone', '$imagePath')";
                        $db->query($query);

                        printf('<img src="data:image/jpj;base64,%s"/ width="400"> <b>phone:  '. $phone .'</b> <br>', base64_encode(ob_get_clean()));

                        imagejpeg($img, $imagePath);
                    }

                    $result = $db->query('SELECT * FROM user');
                    while($row=$result->fetchArray(SQLITE3_ASSOC)){
                        echo 'phone = ' . $row['phone'] . '<br>';
                        echo 'pic = ' . $row['pic'] . '<br>';
                    }
                } else {
                    echo SimpleXLSX::parseError();
                }
            }
        }
    ?>
</body>
</html>